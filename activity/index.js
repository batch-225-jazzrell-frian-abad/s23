let trainer = {
    name: "Ash ketchum",
    age: 10,
    pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
    talk: function(index){
        console.log(this.pokemon[index] + "! I choose you!")
    }
}


console.log(trainer)
console.log("Result of dot notation");
console.log(trainer.name);
console.log("Result of square bracket notation");
console.log(trainer["pokemon"]);


console.log("Result of talk method")
trainer.talk(0);


function Pokemon(name, level)
{
    //Properties 
    this.name = name,
    this.level = level,
    this.health = 2 * level,
    this.attack = level,

    // Method
    this.tackle = function(target){
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        if (target.health <= 0){
        return target.faint(target);
}  
    },

    this.faint = function(dead){
        console.log(this.name + ' fainted.');
    }

}

let pikachu = new Pokemon("Pikachu", 12)
let geodude = new Pokemon("Geodude", 8)
let mewtwo = new Pokemon("MewTwo", 100)



console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
pikachu.tackle(mewtwo);


